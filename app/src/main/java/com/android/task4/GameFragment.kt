package com.android.task4

import android.os.Bundle
import android.util.Log.i
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import com.android.task4.databinding.FragmentGameBinding
import kotlin.math.pow


class GameFragment : Fragment() {

    private lateinit var adapter: RecyclerViewAdapter
    private lateinit var binding: FragmentGameBinding
    private var size: Int = 3
    private var board= mutableMapOf<Int,Int>()

    companion object {
        const val PLAYER_ONE:Int = 1;
        const val PLAYER_TWO:Int = 2;
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
            binding = FragmentGameBinding.inflate(
                inflater, container, false
            )
            init()

        return binding.root
    }

    private fun init() {
        size = arguments?.getInt("size", -1)!!

            adapter = RecyclerViewAdapter(size,object: ButtonListener{
                override fun onClickListener(v: ImageButton, count: Int, adapterPosition1: Int) {
                    setImage(v, count,adapterPosition1)
                }
            })
            binding.recyclerView.layoutManager = GridLayoutManager(context,size)
            binding.recyclerView.adapter = adapter

        binding.btnRestart.setOnClickListener{
            findNavController().navigateUp()
        }
    }

    private fun setImage(v: ImageButton?, count: Int, adapterPosition1: Int) {
        if(count%2==0){
            v?.setImageResource(R.drawable.ic_x_svg)
            v?.isEnabled = false
            if (v != null) {
                board[adapterPosition1]=PLAYER_ONE
            }

            binding.turn.text = "Turn : O"
            if(checkWinner(adapterPosition1, PLAYER_ONE)){
                binding.turn.text = "X Won"

            }else if (count.toDouble() == (size.toDouble().pow(2.0) - 1)) {
                binding.turn.text = "DRAW"
            }
        }else{
            v?.setImageResource(R.drawable.ic_zero_svg)
            v?.isEnabled = false

            if (v != null) {
                board[adapterPosition1]= PLAYER_TWO
            }

            binding.turn.text = "Turn : X"
            if(checkWinner(adapterPosition1, PLAYER_TWO)){
                binding.turn.text = "O Won"
            }else if (count.toDouble() == (size.toDouble().pow(2.0) - 1)) {
                binding.turn.text = "DRAW"
            }

        }
    }

    private fun checkWinner(position: Int, s: Int): Boolean {
        val row: Int = position/size
        val col: Int = position%size

        //check col
        var k = position%size
        for (i in 0 until size) {
            if (board[k] != s) break
            k+=size
            if (i == size - 1) {
                return true
            }
        }

        //check row
        k=(position/size)*size
        for (i in 0 until size) {
            if (board[(i+k)] != s) break
            if (i == size - 1) {
                return true
            }
        }

        //check diag
        if (row == col) {
            //we're on a diagonal
            k=0
            for (i in 0 until size) {
                if (board[k] != s) break
                k+=(size+1)
                if (i == size - 1) {
                    return true

                }
            }
        }

//        //check anti diag (thanks rampion)
        if(row + col == size - 1){
            k=1
            i("pinpoint","anti")
            for (i in 0 until size) {
                if (board[k*(size-1)] != s) break
                k++
                if (i == size - 1) {
                    return true
                }
            }
        }


        return false
    }

    private fun disableButton(){

    }
}