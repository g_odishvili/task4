package com.android.task4

import android.widget.ImageButton

interface ButtonListener {
    fun onClickListener(v: ImageButton, adapterPosition: Int, adapterPosition1: Int);
}