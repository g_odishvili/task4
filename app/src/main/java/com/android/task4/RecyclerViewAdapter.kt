package com.android.task4

import com.android.task4.databinding.ButtonLayoutBinding


import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import androidx.recyclerview.widget.RecyclerView


class RecyclerViewAdapter(
    private val size: Int,
    private val buttonListener: ButtonListener
) : RecyclerView.Adapter<RecyclerViewAdapter.ButtonViewHolder>() {
    private var count = 0;

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ButtonViewHolder {
        return ButtonViewHolder(ButtonLayoutBinding.inflate(LayoutInflater.from(parent.context), parent, false))
    }

    override fun onBindViewHolder(holder: ButtonViewHolder, position: Int) {
         holder.bind()
    }


    override fun getItemCount(): Int  = size * size


    inner class ButtonViewHolder(private val binding: ButtonLayoutBinding)
        : RecyclerView.ViewHolder(binding.root), View.OnClickListener{
        @SuppressLint("SetTextI18n")
        fun bind() {
            binding.btn.setOnClickListener(this)
        }

        override fun onClick(v: View?) {
            buttonListener.onClickListener(v as ImageButton,count++,adapterPosition)
        }
    }
}