package com.android.task4

import android.os.Bundle
import android.util.Log.i
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RadioButton
import androidx.core.os.bundleOf
import androidx.navigation.findNavController
import com.android.task4.databinding.FragmentStartPageBinding


class StartPageFragment : Fragment(){

    private var binding: FragmentStartPageBinding? = null
    private var size: Int = 3
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        if (binding == null) {
            binding = FragmentStartPageBinding.inflate(
                inflater, container, false
            )

            init()
        }
        // Inflate the layout for this fragment
        return binding!!.root
    }

    private fun init() {
        binding?.startGame?.setOnClickListener {
            val bundle = bundleOf("size" to size)
            view?.findNavController()
                ?.navigate(R.id.action_startPageFragment_to_gameFragment, bundle)
        }
        binding?.three?.setOnClickListener {
            size = 3
        }

        binding?.four?.setOnClickListener {
            size = 4
        }

        binding?.five?.setOnClickListener {
            size = 5
        }
    }
}